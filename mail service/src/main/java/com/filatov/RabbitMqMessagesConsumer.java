package com.filatov;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.filatov.client.UserClient;
import com.filatov.model.Notification;
import com.filatov.model.User;
import com.filatov.service.EmailFormatter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Component
@RequiredArgsConstructor
@Slf4j
public class RabbitMqMessagesConsumer {
    private final ObjectMapper objectMapper;
    private final UserClient userClient;
    private final EmailFormatter emailFormatter;
    private final MailSender mailSender;

    @RabbitListener(queues = "${queue.email.name}")
    public void listen(String message) {
        try {
            log.debug(message);
            Notification notification = objectMapper.readValue(message, Notification.class);
            Future<User> creatorFuture = userClient.requestUser(notification.getCreatedById());
            Future<User> assigneeFuture = userClient.requestUser(notification.getAssigneeId());
            User creator = creatorFuture.get();
            User assignee = assigneeFuture.get();
            String mailContent = emailFormatter.formatNotificationEmail(notification, creator, assignee);
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setTo(assignee.getEmail());
            simpleMailMessage.setText(mailContent);
            mailSender.send(simpleMailMessage);
        } catch (Exception e) {
            log.error("Error while sending email", e);
        }
    }
}
