package com.filatov.client;

import com.filatov.client.request.TokenRequest;
import com.filatov.model.JwtToken;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class TokenClient {
    private final RestTemplate restTemplate;

    @Value("${tokenService.endpoint}")
    private String tokenServiceEndpoint;

    @Value("${mailService.email}")
    private String email;

    @Value("${mailService.password}")
    private String password;

    public String requestToken() {
        TokenRequest tokenRequest = new TokenRequest(email, password);
        return restTemplate.postForObject(tokenServiceEndpoint + "/token", tokenRequest, JwtToken.class).getJwt();
    }
}
