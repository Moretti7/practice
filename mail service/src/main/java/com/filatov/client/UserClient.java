package com.filatov.client;

import com.filatov.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
@RequiredArgsConstructor
public class UserClient {
    private final RestTemplate restTemplate;
    private final TokenClient tokenClient;

    @Value("${usersEndpoint}")
    private String usersEndpoint;

    @Async
    public Future<User> requestUser(UUID id) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        String jwtToken = tokenClient.requestToken();
        headers.add("Authorization", "Bearer " + jwtToken);
        HttpEntity<String> entity = new HttpEntity<>("", headers);
        return new AsyncResult<>(restTemplate.exchange(usersEndpoint + "/" + id, HttpMethod.GET, entity, User.class).getBody());
    }
}
