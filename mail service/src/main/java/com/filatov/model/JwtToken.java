package com.filatov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtToken {
    private String jwt;
    private String refreshToken;
    private int expiresIn;
    private int refreshExpiresIn;
}
