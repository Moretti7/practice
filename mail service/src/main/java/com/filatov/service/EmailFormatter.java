package com.filatov.service;

import com.filatov.model.EventType;
import com.filatov.model.Notification;
import com.filatov.model.User;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailFormatter {
    private Map<EventType, String> eventPassiveVerbs = new HashMap<>();

    public EmailFormatter() {
        eventPassiveVerbs.put(EventType.CREATE, "created");
        eventPassiveVerbs.put(EventType.UPDATE, "updated");
    }

    public String formatNotificationEmail(Notification notification, User creator, User assignee) {
        return String.format(
                "Dear %s %s,\n %s %s has %s task for you. Task name: %s. Please check.",
                assignee.getName(), assignee.getSurname(), creator.getName(),  creator.getSurname(),
                eventPassiveVerbs.get(notification.getEventType()),
                notification.getTaskName()
        );
    }
}
