package com.filatov.taskservice.client;

import com.filatov.taskservice.service.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
public class UserClient {
    private final RestTemplate restTemplate;
    private final TokenClient tokenClient;

    @Value("${user.service.search.endpoint}")
    private String userServiceEndpoint;

    public UserDetails findUserByUsername(String email) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        String jwtToken = tokenClient.requestToken();
        headers.add("Authorization", "Bearer " + jwtToken);
        HttpEntity<String> entity = new HttpEntity<>("", headers);
        return restTemplate.exchange(userServiceEndpoint +"?email=" + email, HttpMethod.GET, entity, UserDetailsImpl.class).getBody();
    }
}
