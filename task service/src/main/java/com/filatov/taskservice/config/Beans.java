package com.filatov.taskservice.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Beans {
    @Value("${queue.email.name}")
    private String emailQueueName;

    @Bean
    public Queue emailQueue() {
        System.out.println("emailQueueName = " + emailQueueName);
        return new Queue(emailQueueName);
    }
}
