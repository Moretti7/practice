package com.filatov.taskservice.listener;

import com.filatov.taskservice.model.Task;

public interface TaskCreationListener {
    void handleCreate(Task task);
}
