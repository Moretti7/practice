package com.filatov.taskservice.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.filatov.taskservice.model.EventType;
import com.filatov.taskservice.model.Notification;
import com.filatov.taskservice.model.Task;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TaskCreationListenerImpl implements TaskCreationListener {
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Value("${queue.email.name}")
    private String emailQueueName;

    @SneakyThrows
    @Override
    public void handleCreate(Task task) {
        Notification notification = Notification.builder()
                .assigneeId(task.getAssigneeId())
                .createdById(task.getCreatorId())
                .eventType(EventType.CREATE)
                .taskName(task.getTitle())
                .build();
        rabbitTemplate.convertAndSend(emailQueueName, objectMapper.writeValueAsString   (notification));
    }
}
