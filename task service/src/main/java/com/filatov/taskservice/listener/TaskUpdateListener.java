package com.filatov.taskservice.listener;

import com.filatov.taskservice.model.Task;

public interface TaskUpdateListener {
    void handleUpdate(Task task);
}
