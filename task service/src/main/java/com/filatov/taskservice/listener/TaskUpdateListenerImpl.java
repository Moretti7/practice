package com.filatov.taskservice.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.filatov.taskservice.model.EventType;
import com.filatov.taskservice.model.Notification;
import com.filatov.taskservice.model.Task;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TaskUpdateListenerImpl implements TaskUpdateListener {
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Value("${queue.email.name}")
    private String emailQueueName;

    @SneakyThrows
    @Override
    public void handleUpdate(Task task) {
        Notification notification = Notification.builder()
                .assigneeId(task.getAssigneeId())
                .createdById(task.getCreatorId())
                .eventType(EventType.UPDATE)
                .taskName(task.getTitle())
                .build();
        rabbitTemplate.convertAndSend(emailQueueName, objectMapper.writeValueAsString(notification));
    }
}
