package com.filatov.taskservice.model;

public enum EventType {
    CREATE,
    UPDATE
}
