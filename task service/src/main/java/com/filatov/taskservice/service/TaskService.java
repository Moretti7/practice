package com.filatov.taskservice.service;

import com.filatov.taskservice.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TaskService {
    List<Task> findAll();

    Optional<Task> findById(UUID id);

    Task create(Task task);

    Task update(Task task, UUID id);

    void delete(UUID id);
}
