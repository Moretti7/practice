package com.filatov.taskservice.service;

import com.filatov.taskservice.exception.DeleteException;
import com.filatov.taskservice.exception.UpdateException;
import com.filatov.taskservice.listener.TaskCreationListener;
import com.filatov.taskservice.listener.TaskUpdateListener;
import com.filatov.taskservice.model.Notification;
import com.filatov.taskservice.model.Task;
import com.filatov.taskservice.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository repository;
    private final List<TaskCreationListener> taskCreationListeners;
    private final List<TaskUpdateListener> taskUpdateListeners;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Task> findById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public Task create(Task task) {
        task.setId(UUID.randomUUID());
        task.setCreated(OffsetDateTime.now());
        Task savedTask = repository.save(task);
        taskCreationListeners.forEach(it -> it.handleCreate(task));
        return savedTask;
    }

    @Override
    public Task update(Task task, UUID id) {
        validateId(id);
        Task updatedTask = repository.save(task);
        taskUpdateListeners.forEach(it -> it.handleUpdate(updatedTask));
        return updatedTask;
    }

    @Override
    public void delete(UUID id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
        } else {
            throw new DeleteException("Task not found");
        }
    }

    private void validateId(UUID id) {
        if (!repository.existsById(id)) {
            throw new UpdateException("Task not found");
        }
    }
}
