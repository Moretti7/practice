package com.filatov.jwtprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtProvider {

    public static void main(String[] args) {
        SpringApplication.run(JwtProvider.class, args);
    }

}
