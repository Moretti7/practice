package com.filatov.jwtprovider.controller;

import com.filatov.jwtprovider.dto.JwtToken;
import com.filatov.jwtprovider.dto.UserCredentials;
import com.filatov.jwtprovider.service.TokenService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class TokenController {
    private TokenService tokenService;

    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @PostMapping("/token")
    public ResponseEntity<JwtToken> generateToken(@RequestBody UserCredentials userCredentials) {
        return ResponseEntity.of(tokenService.generateTokenFor(userCredentials));
    }

    @PostMapping("/refresh")
    public ResponseEntity<JwtToken> refreshToken(@RequestParam("refresh_token") String refreshToken) {
        return tokenService.refreshToken(refreshToken)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }
}
