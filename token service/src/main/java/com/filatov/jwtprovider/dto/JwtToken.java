package com.filatov.jwtprovider.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JwtToken {
    private String jwt;
    private String refreshToken;
    private int expiresIn;
    private int refreshExpiresIn;
}
