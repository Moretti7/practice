package com.filatov.jwtprovider.repository;

import com.filatov.jwtprovider.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmailAndPasswordIgnoreCase(String username, String password);

    User findByEmail(String username);
}
