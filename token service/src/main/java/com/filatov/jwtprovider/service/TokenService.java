package com.filatov.jwtprovider.service;

import com.filatov.jwtprovider.dto.JwtToken;
import com.filatov.jwtprovider.dto.UserCredentials;
import com.filatov.jwtprovider.model.User;
import com.filatov.jwtprovider.repository.UserRepository;
import com.filatov.jwtprovider.util.JwtClaimsConstants;
import com.filatov.jwtprovider.util.JwtUtil;
import com.filatov.jwtprovider.util.Passwords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenService {
    private static final Logger LOG = LoggerFactory.getLogger(TokenService.class);

    private UserRepository userRepository;
    private JwtUtil jwtUtil;

    @Value("${token.expiration}")
    private int tokenExpiration;

    @Value("${token.refresh.expiration}")
    private int refreshTokenExpiration;

    public TokenService(UserRepository userRepository, JwtUtil jwtUtil) {
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
    }

    public Optional<JwtToken> generateTokenFor(UserCredentials userCredentials) {
        String emailAndPassword = userCredentials.getEmail();
        String password = Passwords.encryptPassword(userCredentials.getPassword());
        Optional<User> userCandidate = userRepository.findByEmailAndPasswordIgnoreCase(emailAndPassword, password);
        return userCandidate.map(this::createToken);
    }

    public Optional<JwtToken> refreshToken(String refreshToken) {
        JwtToken token = null;

        if (jwtUtil.isRefreshNotExpired(refreshToken)) {
            String username =
                    jwtUtil.extractRefreshClaim(refreshToken, claims -> claims.get(JwtClaimsConstants.EMAIL, String.class));
            User user = userRepository.findByEmail(username);
            token = createToken(user);
        } else {
            LOG.debug("Token {} has been expired", refreshToken);
        }

        return Optional.ofNullable(token);
    }

    private JwtToken createToken(User user) {
        return JwtToken.builder()
                .jwt(jwtUtil.createToken(user))
                .refreshToken(jwtUtil.createRefreshToken(user))
                .expiresIn(tokenExpiration)
                .refreshExpiresIn(refreshTokenExpiration)
                .build();
    }
}
