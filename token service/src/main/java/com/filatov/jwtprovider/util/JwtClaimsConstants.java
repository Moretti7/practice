package com.filatov.jwtprovider.util;

public final class JwtClaimsConstants {
    public static final String ROLE = "role";
    public static final String EMAIL = "email";

    private JwtClaimsConstants() {
        throw new AssertionError();
    }
}
