package com.filatov.jwtprovider.util;

import com.filatov.jwtprovider.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Component
public class JwtUtil {
    private static final long DAYS_TO_PARSE_TOKEN = TimeUnit.DAYS.toSeconds(30);
    @Value("${token.sign.key}")
    private String tokenSignKey;

    @Value("${token.refresh.sign.key}")
    private String refreshTokenSignKey;

    @Value("${token.expiration}")
    private int tokenExpiration;

    @Value("${token.refresh.expiration}")
    private int refreshTokenExpiration;

    public String createToken(User user) {
        Claims claims = Jwts.claims();
        claims.put(JwtClaimsConstants.EMAIL, user.getEmail());

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getEmail())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * tokenExpiration))
                .signWith(SignatureAlgorithm.HS256, tokenSignKey)
                .compact();
    }

    public String createRefreshToken(User user) {
        Claims claims = Jwts.claims();
        claims.put(JwtClaimsConstants.EMAIL, user.getEmail());
        return Jwts.builder()
                .setSubject(user.getEmail())
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * refreshTokenExpiration))
                .signWith(SignatureAlgorithm.HS256, refreshTokenSignKey)
                .compact();
    }

    public <U> U extractClaim(String token, Function<Claims, U> claimExtractor) {
        return extractClaim(token, tokenSignKey, claimExtractor);
    }

    public <U> U extractRefreshClaim(String token, Function<Claims, U> claimExtractor) {
        return extractClaim(token, refreshTokenSignKey, claimExtractor);
    }

    public <U> U extractClaim(String token, String signKey, Function<Claims, U> claimExtractor) {
        Claims claims = extractClaims(token, signKey);
        return claimExtractor.apply(claims);
    }

    public boolean isAccessTokenNotExpired(String token) {
        return isTokenNotExpired(token, tokenSignKey);
    }

    public boolean isRefreshNotExpired(String token) {
        return isTokenNotExpired(token, refreshTokenSignKey);
    }

    private boolean isTokenNotExpired(String token, String tokenKey) {
        Date expiration = extractClaim(token, tokenKey, Claims::getExpiration);
        return Date.from(Instant.now()).before(expiration);
    }

    private Claims extractClaims(String token, String signKey) {
        return Jwts.parser()
                .setSigningKey(signKey)
                .setAllowedClockSkewSeconds(DAYS_TO_PARSE_TOKEN)
                .parseClaimsJws(token)
                .getBody();
    }
}
