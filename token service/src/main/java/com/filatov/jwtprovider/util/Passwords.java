package com.filatov.jwtprovider.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Passwords {
    public static String encryptPassword(String password) {
        String result = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encryptedBytes = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            result = toHex(encryptedBytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return result.toUpperCase();
    }

    private static String toHex(byte[] digest) {
        StringBuilder sb = new StringBuilder();
        int byteValue;

        for (int i = 0; i < digest.length; i++) {
            if (digest[i] < 0) {
                byteValue = 256 + digest[i];
            } else {
                byteValue = digest[i];
            }

            if (byteValue < 16) {
                sb.append('0').append(Integer.toHexString(byteValue));
            } else {
                sb.append(Integer.toHexString(byteValue));
            }
        }
        return sb.toString();
    }
}
