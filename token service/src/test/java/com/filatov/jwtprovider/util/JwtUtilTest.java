package com.filatov.jwtprovider.util;

import com.filatov.jwtprovider.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ContextConfiguration(classes = JwtUtil.class)
class JwtUtilTest {
    @Autowired
    private JwtUtil jwtUtil;

    @Value("${token.sign.key}")
    private String key;

    @Test
    public void createToken_tokenNotNull() {
        String token = jwtUtil.createToken(User.builder().email("test-user").build());
        System.out.println(token);
        assertNotNull(token);
    }

    @Test
    public void isValid_tokenExpirationIsAfterNow_true() {
        String token = Jwts.builder()
                .setExpiration(Date.from(Instant.now().plus(1, ChronoUnit.DAYS)))
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();

        assertTrue(jwtUtil.isAccessTokenNotExpired(token));
    }

    @Test
    public void isValid_tokenExpirationIsBeforeNow_false() {
        String token = Jwts.builder()
                .setExpiration(Date.from(Instant.now().minus(1, ChronoUnit.DAYS)))
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();

        assertFalse(jwtUtil.isAccessTokenNotExpired(token));
    }
}
