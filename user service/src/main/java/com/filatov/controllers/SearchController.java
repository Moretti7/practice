package com.filatov.controllers;

import com.filatov.model.User;
import com.filatov.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/search")
@RequiredArgsConstructor
public class SearchController {
    private final UserService userService;

    @GetMapping("/users")
    public ResponseEntity<User> findByEmail(@RequestParam("email") String email) {
        return ResponseEntity.of(userService.findByEmail(email));
    }
}
