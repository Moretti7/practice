package com.filatov.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.filatov.model.User;
import com.filatov.service.UserService;
import com.filatov.views.Views;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;


    @GetMapping
    @JsonView(Views.Common.class)
    public ResponseEntity<List<User>> get() {
        return ResponseEntity.ok(userService.findAll());
    }

    @GetMapping("/{id}")
    @JsonView(Views.Common.class)
    public ResponseEntity<User> findById(@PathVariable UUID id) {
        return ResponseEntity.of(userService.findByUuid(id));
    }

    @PostMapping
    @JsonView(Views.Common.class)
    public User create(@RequestBody User user) {
        return userService.create(user);
    }

    @PutMapping("/{id}")
    @JsonView(Views.Common.class)
    public User update(@PathVariable UUID id, @RequestBody User user) {
        return userService.update(id, user);
    }

    @DeleteMapping("/{id}")
    @JsonView(Views.Common.class)
    public void delete(@PathVariable UUID id) {
        userService.deleteById(id);
    }
}
