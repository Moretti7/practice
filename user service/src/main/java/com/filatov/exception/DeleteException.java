package com.filatov.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DeleteException extends ResponseStatusException {
    public DeleteException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
