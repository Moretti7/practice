package com.filatov.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UpdateException extends ResponseStatusException {
    public UpdateException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
