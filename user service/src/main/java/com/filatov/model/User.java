package com.filatov.model;

import com.fasterxml.jackson.annotation.JsonView;
import com.filatov.views.Views;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
@JsonView(Views.Common.class)
public class User {
    @Id
    private UUID id;
    private String name;
    private String surname;
    private String email;

    @JsonView(Views.Private.class)
    @Column(updatable = false)
    private String password;
}
