package com.filatov.service;

import com.filatov.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    List<User> findAll();

    Optional<User> findByUuid(UUID uuid);

    User update(UUID id, User user);

    User create(User user);

    void deleteById(UUID id);

    Optional<User> findByEmail(String email);
}
