package com.filatov.service;

import com.filatov.exception.DeleteException;
import com.filatov.exception.UpdateException;
import com.filatov.model.User;
import com.filatov.repository.UserRepository;
import com.filatov.util.Passwords;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findByUuid(UUID uuid) {
        return userRepository.findById(uuid);
    }

    @Override
    public User update(UUID id, User user) {
        validateUserId(id);
        return userRepository.save(user);
    }

    private void validateUserId(UUID id) {
        if (!userRepository.existsById(id)) {
            throw new UpdateException("User not found");
        }
    }

    @Override
    public User create(User user) {
        user.setId(UUID.randomUUID());
        user.setPassword(Passwords.encryptPassword(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public void deleteById(UUID id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            throw new DeleteException("User not found");
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
