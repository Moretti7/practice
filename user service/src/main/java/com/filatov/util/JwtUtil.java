package com.filatov.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Component
public class JwtUtil {
    private static final long DAYS_TO_PARSE_TOKEN = TimeUnit.DAYS.toSeconds(30);
    @Value("${token.sign.key}")
    private String tokenSignKey;

    @Value("${token.refresh.sign.key}")
    private String refreshTokenSignKey;

    public <U> U extractClaim(String token, String signKey, Function<Claims, U> claimExtractor) {
        Claims claims = extractClaims(token, signKey);
        return claimExtractor.apply(claims);
    }

    public boolean isAccessTokenNotExpired(String token) {
        return isTokenNotExpired(token, tokenSignKey);
    }

    public boolean isRefreshNotExpired(String token) {
        return isTokenNotExpired(token, refreshTokenSignKey);
    }

    public String extractUsername(String token) {
        return extractClaim(token, tokenSignKey, Claims::getSubject);
    }

    private boolean isTokenNotExpired(String token, String tokenKey) {
        Date expiration = extractClaim(token, tokenKey, Claims::getExpiration);
        return Date.from(Instant.now()).before(expiration);
    }

    private Claims extractClaims(String token, String signKey) {
        return Jwts.parser()
                .setSigningKey(signKey)
                .setAllowedClockSkewSeconds(DAYS_TO_PARSE_TOKEN)
                .parseClaimsJws(token)
                .getBody();
    }
}
